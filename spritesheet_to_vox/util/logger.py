from enum import Enum
import inspect

class LogLevel(Enum):
    INFO        = " [INFO] "
    WARNING     = " [WARN] "
    ERROR       = " [ERROR] "
    CRITICAL    = " [CRITICAL] "

def log(message : str, level : LogLevel = LogLevel.INFO, pad : int = 0):
    stack = inspect.stack()[1]
    class_name = str(stack[0].f_locals["self"].__class__.__name__)
    pad_str = ""
    for i in range(pad):
        pad_str += "  "
    print(pad_str + level.value + "[" + class_name + "] " +  message)