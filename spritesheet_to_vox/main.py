import sys, os, ntpath as nt
sys.path.append(os.path.join(nt.split(os.path.abspath(__file__))[0], ".."))
from spritesheet_to_vox.interface.stvinterface import StvInterfaceApp as app

if __name__ == "__main__":
    print(sys.path)
    app().run()