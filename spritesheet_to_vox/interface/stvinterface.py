# Core library imports
import getpass
import os
from enum import Enum
from typing import List
import cv2

# UI imports
import kivy

kivy.require("1.11.1")
from kivy.app import App
from kivy.factory import Factory
from kivy.clock import mainthread
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.popup import Popup
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.graphics.texture import Texture

# Internal application imports
from spritesheet_to_vox.spritesheet_splitter.spritesheetsplitter import SpritesheetSplitter
from spritesheet_to_vox.spritesheet_splitter.util import splitterutils as util, splitterconfig as cfg
from spritesheet_to_vox.util import logger

s_COLOR_FMT = "bgra"
s_BUFFER_FMT = "ubyte"
t_NO_ERR_COLOR = (0.5, 0.5, 0.5, 1)
t_ERR_COLOR = (1, 0, 0, 1)

def convert_to_texture(image : cv2) -> Texture:
    # Kivy's texture flip methods are theoretically less demanding, but they don't actually work.
    image = cv2.flip(image, 0)
    buffer = image.tostring()
    texture : Texture = Texture.create(size=(image.shape[1], image.shape[0]), colorfmt=s_COLOR_FMT)
    texture.blit_buffer(buffer, colorfmt=s_COLOR_FMT, bufferfmt=s_BUFFER_FMT)
    texture.min_filter = 'nearest'
    texture.mag_filter = 'nearest'
    return texture

class ProcessingError(Enum):
    NO_ERROR = "No errors detected."
    NO_FILE = "No file loaded!"
    TILE_SIZE_INVALID = "Invalid tile size! Use an integer greater than zero."
    TILE_SCALE_INVALID = "Invalid tile scale! Use an integer greater than zero."
    ALPHA_INVALID = "Invalid overlay alpha! Use a float from 0.0-1.0."
    FILE_TYPE_UNSUPPORTED = ("Invalid file format! Please use an OpenCV-supported image format:\n" +
                                util.print_file_types())
    OUTPUT_DIR_EMPTY = "The output directory name cannot be empty."

class SplitImage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.images : List[Image] = []

    def clear(self):
        self.clear_widgets()
        self.images = []

    def populate(self, new_images : List[Texture], original : Texture,
                 tile_width : int, tile_height : int):
        self.populate(new_images, original.width, original.height, tile_width, tile_height)

    def populate(self, new_images : List[Texture], original_width :int , original_height : int,
                 tile_width : int, tile_height : int):
        self.images = new_images
        self.clear_widgets()
        self.cols = (original_width // tile_width)
        self.rows = (original_height // tile_height)
        logger.log("Loaded split image with " + str(self.cols) + " columns and " + str(self.rows) + " rows.")
        for x in range(self.cols):
            for y in range(self.rows):
                logger.log("")
                index = x * self.rows + y
                logger.log("Populated at index " + str(index), pad = 1)
                if index == len(self.images):
                    logger.log("Index " + str(index) + " out of bounds. Aborting populate.",
                               logger.LogLevel.ERROR)
                    return
                img : Image = Image()
                img.allow_stretch = True
                img.keep_ratio = True
                img.size_hint = (1, 1)
                img.width = (self.width // self.cols)
                img.height = (self.height // self.rows)
                img.texture = self.images[x * self.rows + y]
                self.add_widget(img)

class FileSelector(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    dirselect = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.default_path = ""
        for root, dirs, files in os.walk("/Users"):
            dir_found = False
            if not root.lower().__contains__(getpass.getuser().lower()):
                continue
            for name in dirs:
                if name == "Desktop":
                    self.default_path = os.path.join(root, name)
                    dir_found = True
                    break
            if dir_found:
                break
        self.ids.filechooser.path = self.default_path

class SplitterInterface(FloatLayout):
    output_directory_field : ObjectProperty = ObjectProperty(None)
    file_type_field : ObjectProperty = ObjectProperty(None)
    join_sequence_field : ObjectProperty = ObjectProperty(None)
    tile_width_field : ObjectProperty = ObjectProperty(None)
    tile_height_field : ObjectProperty =  ObjectProperty(None)
    tile_scale_field : ObjectProperty = ObjectProperty(None)
    overlay_alpha_field : ObjectProperty = ObjectProperty(None)
    error_field : ObjectProperty = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(SplitterInterface, self).__init__(**kwargs)
        self.splitter : SpritesheetSplitter = SpritesheetSplitter(False)
        self.cfg : cfg.SplitterConfig = self.splitter.config
        self.loaded_image : Texture = Texture.create()
        self.overlaid_image : Texture = Texture.create()
        self.popup : Popup = None

        @mainthread
        def delayed():
            self.output_directory_field.text = self.cfg.output_directory
            self.file_type_field.text = self.cfg.file_type
            self.join_sequence_field.text = self.cfg.join_seq
            self.tile_width_field.text = str(self.cfg.tile_width)
            self.tile_height_field.text = str(self.cfg.tile_height)
            self.tile_scale_field.text = str(self.cfg.tile_scale)
            self.overlay_alpha_field.text = str(self.cfg.overlay_alpha)
        delayed()

    def close_popup(self):
        self.popup.dismiss()

    def show_load(self):
        content = FileSelector(load = self.load, cancel = self.close_popup)
        self.popup = Popup(title = "Select a file...", content = content, size_hint = (0.9, 0.9))
        self.popup.open()

    def show_dir_select(self):
        content = FileSelector(load=self.select_dir, cancel=self.close_popup, dirselect=True)
        self.popup = Popup(title = "Select a directory...", content = content, size_hint = (0.9, 0.9))
        self.popup.open()

    def select_dir(self, path):
        self.output_directory_field.text = str(path[0])
        self.close_popup()

    def load(self, filename):
        self.splitter.files.clear() # we're only working with one file at a time via GUI
        self.file_path : str = str(filename[0])
        self.splitter.add(self.file_path)
        self.close_popup()
        logger.log("Interface filename: " + self.file_path)
        # If the file chosen exists, set the "input field" to display its name
        if self.file_path:
            self.ids.splitter_get_file.text = self.file_path

        # convert the chosen image to a texture and assign it to the displayed image
        self.loaded_image = convert_to_texture(cv2.imread(self.file_path, cv2.IMREAD_UNCHANGED))
        if self.loaded_image:
            self.ids.loaded_image.texture = self.loaded_image
            self.ids.overlaid_image.texture = None
            self.ids.split_image.clear()

    def process_split(self):
        if len(self.splitter.files) == 0:
            self.error_display(ProcessingError.NO_FILE)
            return

        tw_valid = util.valid_integer(self.tile_width_field.text)
        th_valid = util.valid_integer(self.tile_height_field.text)
        ts_valid = util.valid_integer(self.tile_scale_field.text)
        oa_valid = util.valid_alpha(self.overlay_alpha_field.text)

        if not tw_valid[1] or not th_valid[1]:
            self.error_display(ProcessingError.TILE_SIZE_INVALID)
            return
        elif not ts_valid[1]:
            self.error_display(ProcessingError.TILE_SCALE_INVALID)
            return
        elif not oa_valid[1]:
            self.error_display(ProcessingError.ALPHA_INVALID)
            return

        # remove the old split file if there is one
        self.splitter.split_files.clear()
        # split the image up
        self.splitter.process_split(tw_valid[0], th_valid[0], ts_valid[0], oa_valid[0])
        # the first element here refers to the files from the provided image. outside console/script use,
        # this should never have more than one element.
        split_files = self.splitter.split_files[0]
        overlay = split_files[len(split_files) - 1]
        self.overlaid_image = convert_to_texture(overlay.tile_data)

        logger.log("Split files: " + str(len(split_files)))
        logger.log("Overlay file: " + overlay.image_name)
        logger.log("Populating split textures...")

        split_textures = []
        for image in split_files:
            logger.log("Loaded: " + image.image_name, pad = 1)
            split_textures.append(convert_to_texture(image.tile_data))
        self.ids.split_image.populate(split_textures, self.loaded_image.width, self.loaded_image.height,
                                  8, 8)

        if self.overlaid_image:
            self.ids.overlaid_image.texture = self.overlaid_image

    def write_out(self):
        if len(self.splitter.split_files) == 0:
            self.error_display(ProcessingError.NO_FILE)
            return

        file_type = self.file_type_field.text
        output_dir = self.output_directory_field.text

        if file_type not in util.a_ACCEPTABLE_FORMATS:
            self.error_display(ProcessingError.FILE_TYPE_UNSUPPORTED)
            return
        elif len(output_dir) == 0:
            self.error_field(ProcessingError.OUTPUT_DIR_EMPTY)
            return

        self.splitter.write_out(self.splitter.split_files[0], self.splitter.files[0], file_type, output_dir)

    def error_display(self, error_type : ProcessingError):
        self.error_field.text = str(error_type.value)
        if error_type == ProcessingError.FILE_TYPE_UNSUPPORTED:
            self.error_field.text = self.error_field.text + "\n" + util.print_file_types()
        self.error_field.foreground_color = t_NO_ERR_COLOR if error_type == ProcessingError.NO_ERROR \
            else t_ERR_COLOR

class InterfaceBase(TabbedPanel):
    pass

class StvInterfaceApp(App):
    def build(self):
        return InterfaceBase()

Factory.register("FileSelector", cls=FileSelector)
Factory.register("SplitterInterface", cls=SplitterInterface)
Factory.register("SplitImage", cls=SplitImage)