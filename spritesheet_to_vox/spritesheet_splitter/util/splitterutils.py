from typing import Tuple

VALID_YN_QUERIES = {"y"         : True,
                    "ye"        : True,
                    "yes"       : True,
                    "yep"       : True,
                    "yee"       : True,
                    "yeah"      : True,
                    "n"         : False,
                    "no"        : False,
                    "nah"       : False,
                    "nope"      : False}

# I'm not sure why you'd be working with MOST of these, but I thought I'd add them anyway for warnings' sake.
a_ACCEPTABLE_FORMATS : list = ["bmp", "dib",                # Windows bitmaps
                               "jpg", "jpeg",               # JPEG
                               "jp2",                       # JPEG-2000
                               "png",                       # Portable Network Graphics
                               "webp",                      # WebP
                               "pbm", "pgm", "ppm", "pnm",  # Portable image format
                               "sr", "ras",                 # Sun rasters
                               "tiff", "tif",               # TIFF
                               "exr",                       # OpenEXT image
                               "hdr", "pic"]                # Radiance HDR

# total OCD shit for printing supported formats
i_FORMAT_DISPLAY_COLS = 3       # number of formats per row to display
i_FORMAT_DISPLAY_PADDING = 6    # string padding size

def cmd_as_positive_integer(prompt : str, default_value : int) -> int:
    while True:
        try:
            val = cmd_prompt_str(prompt, default_value)
            if (len(val) == 0 or val == "") and default_value > 0:
                return default_value
            return abs(int(val))
        except ValueError:
            print("Please enter an integer.")

def valid_integer(val : str) -> Tuple[int, bool]:
    try:
        return (abs(int(val)), abs(int(val)) > 0)
    except ValueError:
        return (1, False)

def valid_alpha(val : str) -> Tuple[float, bool]:
    try:
        float_val : float = abs(float(val))
        return (float_val, float_val >= 0.0 and float_val <= 1.0)
    except ValueError:
        return (0.0, False)

def cmd_as_alpha(prompt : str, default_value : float) -> float:
    while True:
        try:
            val = cmd_prompt_str(prompt, default_value)
            if (len(val) == 0 or val == "") and default_value >= 0.0:
                return default_value
            else:
                valid = valid_alpha(val)
                if not valid[1]:
                    raise ValueError
                return valid[0]
        except ValueError:
            print("Please enter a float between 0.0 and 1.0.")

def cmd_prompt_str(prompt : str, default_value) -> str:
    val = input(prompt_with_default(prompt, default_value))
    if len(val) == 0 or val == "":
        val = str(default_value)
    return val

def prune_extension(filename : str) -> str:
    """
    Prunes the file extension off a string. Shorthand for rsplit(".", 1"[0].
    Args:
        filename:   The string to trim.

    Returns:
                    The filename, without the extension.

    """
    return filename.rsplit(".", 1)[0]

def cmd_query_yes_no(prompt : str, default : str = "yes") -> bool:
    """
    Queries a yes/no from the user and returns the appropriate boolean value.
    Modeled after the package installer.
    Args:
        prompt:     A string prompt to include before the [y/n] prompt.
        default:    The default value to respond with (

    Returns:        True if the response is some variant of 'yes', false if some variant of 'no'.
    """
    default = default.lower()
    display_yes_no : str = " [Y/n]: "
    if default in VALID_YN_QUERIES:
        if not VALID_YN_QUERIES[default]:
            display_yes_no = display_yes_no.swapcase()
    elif default is None:
        display_yes_no = display_yes_no.lower()
    else:
        raise ValueError

    prompt = prompt + display_yes_no
    while True:
        choice : str = input(prompt).lower()
        if choice == "":
            if default is None or default not in VALID_YN_QUERIES:
                print("Default value not set or invalid. Please enter a value.")
                continue
            else:
                choice = default
        else:
            if choice not in VALID_YN_QUERIES:
                print("Invalid reply.")
                continue
        return VALID_YN_QUERIES[choice]

def prompt_with_default(prompt : str, default_value) -> str:
    return prompt + " (enter for " + str(default_value) + "): "

def cmd_check_file_type(file_type : str) -> str:
    """
    Checks if a file type is supposed. A bit too simple.
    Args:
        file_type: The string representation of the file extension.
    Returns:
        The original extension if it's supported, or a user-selected one if not.
    """
    while file_type not in a_ACCEPTABLE_FORMATS:
        if file_type not in a_ACCEPTABLE_FORMATS:
            print(file_type + " is not a supported format. Supported formats:\n" +
                  print_file_types())
        file_type = cmd_prompt_str("File type", file_type)
    return file_type

def print_file_types() -> str:
    output : str = ""
    index: int = 0
    for extension in a_ACCEPTABLE_FORMATS:
        output += (str.ljust(extension, i_FORMAT_DISPLAY_PADDING, " "))
        index += 1
        if index == i_FORMAT_DISPLAY_PADDING:
            index = 0
            output += "\n"
    return output