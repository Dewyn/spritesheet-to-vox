import ntpath as nt
import os
from enum import Enum
from pathlib import Path
import yaml
import spritesheet_to_vox.spritesheet_splitter.util.splitterutils as util
from spritesheet_to_vox.util import logger

s_CONFIG_FILE : str = os.path.join("..", "config.yml")  # name of the config file to search for on init

class ConfigPath(Enum):
    OUTPUT_DIRECTORY_NAME   : (str, str)    = ("output-directory", "split")
    FILE_TYPE               : (str, str)    = ("file-type", "png")
    JOIN_SEQ                : (str, str)    = ("join-seq", "_")
    TILE_WIDTH              : (str, int)    = ("tile-width", 8)
    TILE_HEIGHT             : (str, int)    = ("tile-height", 8)
    TILE_SCALE              : (str, int)    = ("tile-scale", 1)
    OVERLAY_ALPHA           : (str, float)  = ("overlay-alpha", 0.3)

class SplitterConfig:
    def __init__(self):
        self.cfgpath = os.path.join(os.path.abspath(nt.split(__file__)[0]), s_CONFIG_FILE)
        self.load_config()

    def load_config(self):
        if not os.path.isfile(self.cfgpath):
            Path(self.cfgpath).touch()

        # read values from config - if they don't exist, set from default
        with open(self.cfgpath) as stream:
            self.yaml_config_dict               = yaml.safe_load(stream)
            self.output_directory   : str       = self.value_for(ConfigPath.OUTPUT_DIRECTORY_NAME)
            if self.output_directory == "":
                logger.log("Configured output directory is invalid. Writing default: "
                           + str(ConfigPath.OUTPUT_DIRECTORY_NAME.value[1]),
                                 logger.LogLevel.WARNING)
            self.file_type          : str       = self.value_for(ConfigPath.FILE_TYPE)
            if self.file_type not in util.a_ACCEPTABLE_FORMATS:
                logger.log("Configured file type is invalid. Writing default: "
                           + str(ConfigPath.FILE_TYPE.value[1]),
                           logger.LogLevel.WARNING)
            self.join_seq           : str       = self.value_for(ConfigPath.JOIN_SEQ)

            # numerical values
            alpha_valid = util.valid_alpha(self.value_for(ConfigPath.OVERLAY_ALPHA))
            if not alpha_valid[1]:
                logger.log("Configured alpha is invalid. Writing default: "
                           + str(ConfigPath.OVERLAY_ALPHA.value[1]),
                           logger.LogLevel.WARNING)
                alpha_valid = (ConfigPath.OVERLAY_ALPHA.value[1], True)

            tw_valid = util.valid_integer(self.value_for(ConfigPath.TILE_WIDTH))
            if not tw_valid[1]:
                logger.log("Configured tile width is invalid. Writing default: "
                           + str(ConfigPath.TILE_WIDTH.value[1]),
                           logger.LogLevel.WARNING)
                tw_valid = (ConfigPath.TILE_WIDTH.value[1], True)

            th_valid = util.valid_integer(self.value_for(ConfigPath.TILE_HEIGHT))
            if not th_valid[1]:
                logger.log("Configured tile height is invalid. Writing default: "
                           + str(ConfigPath.TILE_HEIGHT.value[1]),
                           logger.LogLevel.WARNING)
                th_valid = (ConfigPath.TILE_HEIGHT.value[1], True)

            ts_valid = util.valid_integer(self.value_for(ConfigPath.TILE_SCALE))
            if not ts_valid[1]:
                logger.log("Configured tile scale is invalid. Writing default: "
                           + str(ConfigPath.TILE_SCALE.value[1]),
                           logger.LogLevel.WARNING)
                ts_valid = (ConfigPath.TILE_SCALE.value[1], True)

            self.tile_width         : int       = tw_valid[0]
            self.tile_height        : int       = th_valid[0]
            self.tile_scale         : int       = ts_valid[0]
            self.overlay_alpha      : float     = alpha_valid[0]
            stream.close()

        # After reading, save the config in case any default values need to be written to.
        self.save_config()

    def save_config(self):
        config: dict = {
            ConfigPath.OUTPUT_DIRECTORY_NAME.value[0]   : self.output_directory,
            ConfigPath.FILE_TYPE.value[0]               : self.file_type,
            ConfigPath.JOIN_SEQ.value[0]                : self.join_seq,
            ConfigPath.TILE_WIDTH.value[0]              : self.tile_width,
            ConfigPath.TILE_HEIGHT.value[0]             : self.tile_height,
            ConfigPath.TILE_SCALE.value[0]              : self.tile_scale,
            ConfigPath.OVERLAY_ALPHA.value[0]           : self.overlay_alpha
        }

        with open(self.cfgpath, "w") as stream:
            yaml.safe_dump(config, stream)
            stream.close()
        logger.log("Wrote config to " + self.cfgpath)

    def value_for(self, cfg : ConfigPath):
        try:
            return self.yaml_config_dict.get(cfg.value[0], cfg.value[1])
        except AttributeError:
            logger.log("Config value for " + cfg.value[0] + " does not exist. Writing with default: "
                       + str(cfg.value[1]),
                       logger.LogLevel.WARNING)
            return cfg.value[1]

if __name__ == "__main__":
    SplitterConfig()    # used for testing. Generates or repairs the config.yml.