import ntpath as nt
import os
import sys
from typing import List

import cv2
import numpy as np

# modification required if running from command line because i suck at packages
if __name__ == "__main__":
    sys.path.append(os.path.join(nt.split(os.path.abspath(__file__))[0], "..", ".."))
from spritesheet_to_vox.spritesheet_splitter.util import splitterconfig as cfg, splitterutils as util
from spritesheet_to_vox.util import logger

class ImageSlice:
    def __init__(self, tile_data, image_name):
        self.tile_data = tile_data
        self.image_name = image_name

class SpritesheetSplitter:
    def __init__(self, as_script : bool):
        self.files          : List[str]                 = []
        self.split_files    : List[List[ImageSlice]]    = []
        self.config         : cfg.SplitterConfig        = cfg.SplitterConfig()
        self.as_script      : bool                      = as_script

        logger.log("Running as script.") if as_script else logger.log("Running from another class.")

        if as_script:
            # Initialize input files.
            # If the script is run with system-provided files:
            if len(sys.argv) > 1:
                logger.log("Found " + str(len(sys.argv) - 1) + " file(s) from arguments:")
                for i in range(1, len(sys.argv)):
                    filename = sys.argv[i]  # file passed via cmd line arg or drag/drop
                    self.add(filename)      # add the file to the list of files to parse

            # Otherwise, from command line.
            else:
                while True:
                    logger.log("Current directory: " + os.getcwd())
                    filename = input("Filename: ")  # user inputs own filename
                    self.add(filename)
                    if not util.cmd_query_yes_no("Process more files?"):
                        break
            self.process_split(self.config.tile_width, self.config.tile_height, self.config.tile_scale,
                               self.config.overlay_alpha, self.config.output_directory, self.config.file_type)

    def add(self, filename : str):
        """
        Adds a file to the processing list by filename.

        If the file isn't found, it's ignored.
        Args:
            filename: The file to try and add.
        """
        # strip off quotation marks added by Windows drag/drop inputs and resolve to absolute path
        filename = os.path.abspath(filename.replace("\"", ""))
        logger.log("Attempting to add: " + filename)
        # if not a file, do nothing
        if not os.path.isfile(filename):
            logger.log(filename + " is not a valid file, ignoring.", logger.LogLevel.ERROR)
            return
        # otherwise add it to the list to process
        self.files.append(filename)
        logger.log("Added file: " + filename)

    def process_split(self, tile_width : int, tile_height : int,
                      tile_scale : int, overlay_alpha : float,
                      output_dir : str = "", file_type : str = ""):
        """
        Displays provided settings, asks if this is a batch operation, then processes all files.
        """
        # if this is False, then the split method will ask for user-input values for all submitted files.
        # otherwise, it'll use the provided ones.
        batch = True
        if self.as_script:
            logger.log("Default settings:")
            logger.log("Tile width: " + str(tile_width))
            logger.log("Tile height: " + str(tile_height))
            logger.log("Tile scale: " + str(tile_scale))
            logger.log("Overlay alpha: " + str(overlay_alpha))
            logger.log("Running as script.")
            batch = util.cmd_query_yes_no("Use for all?")
        for i in range(len(self.files)):
            # the filename, because each ImageSlice has its own name.
            # by default, this name is the image filename plus its coordinates.
            file : str = util.prune_extension(nt.split(self.files[i])[1])
            tiles = self.split(self.files[i], file, tile_width, tile_height,
                                                tile_scale, overlay_alpha,
                                                batch)
            self.split_files.append(tiles)
        # stop here if running from UI - someone may want to mess with the splitter settings and re-process,
        # or modify export settings before exporting.

        # If running from script, proceed to the next step: writing
        if self.as_script:
            logger.log("Write settings:")
            logger.log("Output directory: " + output_dir)
            file_type = util.cmd_check_file_type(file_type)
            logger.log("File type: " + file_type)
            batch = util.cmd_query_yes_no("Use for all?")
            for i in range(len(self.split_files)):
                tiles = self.split_files[i]
                logger.log("Processing split for " + self.files[i])
                if not batch:
                    output_dir = util.cmd_prompt_str("Output directory:", output_dir)
                    file_type = util.cmd_check_file_type(util.cmd_prompt_str("File type:", file_type))
                self.write_out(tiles, self.files[i], file_type, output_dir)

    def split(self, filename : str, file : str,
              tile_width : int, tile_height : int, tile_scale : int,
              overlay_alpha : float, batch : bool = False) -> List[ImageSlice]:
        """
        Splits the image provided by filename up into tiles according to the configured (or prompted)
        tile sizes and outputs those sliced images to their own files in their own directory.

        Also outputs a copy of the original with the outline of each sliced area highlighted.
        Green means that the contents of that tile were passed as their own file.
        Red means that the contents of that tile were discarded (either solid black/white or fully transparent).
        Args:
            filename:   The path to the image to split.
            file:       The default name for each ImageSlice.
            batch:      Whether or not this is a batch operation. "True" will skip prompts for tile size, file type,
                        and overlay opacity.
            returns:    A list of ImageSlice objects, each containing tile data and a name for that image.
        """
        split_output : list = []

        try:
            image = cv2.imread(filename, cv2.IMREAD_UNCHANGED)       # The original image to load.
        except:
            logger.log("Unable to read file. Aborting.", logger.LogLevel.ERROR)
            return split_output
        overlay = image.copy()                                       # An image to draw on for user troubleshooting.
        output  = image.copy()                                       # The overlaid image to display.

        # load the relevant image channels
        height  = image.shape[0]
        width   = image.shape[1]

        logger.log("Current file: " + file)

        # if this is not being done as a batch operation. ask to specify for each file
        if not batch and self.as_script:
            tile_width       = util.cmd_as_positive_integer("Tile width", tile_width)
            tile_height      = util.cmd_as_positive_integer("Tile height", tile_height)
            tile_scale       = util.cmd_as_positive_integer("Tile scale", tile_scale)
            overlay_alpha    = util.cmd_as_alpha("Overlay alpha", overlay_alpha)

        n : int = (width // tile_width) // 2     # columns (horizontal tiles)
        m : int = (height // tile_height) // 2   # rows (vertical tiles)
        logger.log("Image size: " + str(width) + " by " + str(height), pad = 1)
        logger.log("Tile size: " + str(tile_width * tile_scale) + " by " + str(tile_height * tile_scale), pad = 1)
        logger.log("Tiles: " + str(n) + " by " + str(m), pad = 1)

        # slice the image into tiles:
        for y in range(0, height, tile_height):
            for x in range(0, width, tile_width):
                # endpoints of the rect to slice
                pY = y + tile_height
                pX = x + tile_width

                # If the endpoints are outside the bounds of the image, set them inside.
                if pY > height:
                    pY = height
                if pX > width:
                    pX = width

                tiles = image[y:pY, x:pX, :]    # The relevant slice of the image, including full alpha.
                solidTest = cv2.cvtColor(tiles, cv2.COLOR_BGR2GRAY) # As a single-channel grayscale image.
                solid : bool = cv2.countNonZero(solidTest) == 0

                # if there's even one nontransparent pixel and it's not solid black, write the sliced image
                # only calls when run from script - gui parses whole image
                if (np.any(tiles[2]) and not solid) or not self.as_script:
                    # add this slice to the output array
                    img_name = file + self.config.join_seq + str(x) + self.config.join_seq + str(y)
                    logger.log("Sliced: " + img_name, pad = 2)
                    tiles = cv2.resize(tiles, (tiles.shape[1] * tile_scale, tiles.shape[0] * tile_scale),
                                       interpolation=cv2.INTER_NEAREST)
                    split_output.append(ImageSlice(tiles, img_name))

                rectColor = (0, 255, 0, 255) if not solid else (0, 0, 255, 255)
                # Draw a rect around the sliced area.
                cv2.rectangle(overlay, (x, y), (pX - 1, pY - 1), rectColor)

        # Create an overlaid image to show sliced boundaries:
        cv2.addWeighted(overlay, overlay_alpha, image, 1.0 - overlay_alpha, 0, output)
        # determine the overlay path for this image
        overlay_name : str = file + self.config.join_seq + "split"
        # add the overlaid image to the end of the output array
        split_output.append(ImageSlice(output, overlay_name))
        logger.log("Generated overlay: " + overlay_name, pad = 2)
        return split_output

    def write_out(self, tiles : List[ImageSlice], original_filename : str, file_type : str, output_dir : str = ""):
        path, file = nt.split(original_filename)
        file = file.rsplit(".", 1)[0]
        if not os.path.isabs(output_dir):
            output_dir = os.path.join(path, output_dir)
        output_dir = os.path.join(output_dir, file)
        for img in tiles:
            if not os.path.isdir(output_dir):
                logger.log("Creating directory: " + output_dir)
                os.makedirs(output_dir)
            file_path = os.path.join(output_dir, img.image_name + "." + file_type)
            logger.log("Writing: " + file_path, pad = 1)
            cv2.imwrite(file_path, img.tile_data)
        logger.log("Finished writing.")

# If running from command line or via drag-and-drop.
if __name__ == "__main__":
    splitter = SpritesheetSplitter(True)